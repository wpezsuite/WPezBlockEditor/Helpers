## WPezBlockEditor \ Helpers \ BlockAdvanced

__A class used to customize the WordPress Block Editor UI / UX. The ezWay to remove the Advanced > Anchor and Advanced > Additional CSS Class controls.__


### Overview

At this time, the Block's Advance settings - HTML Anchor control and Additional CSS Class control - can not be manipulated (read: removed) via the theme.json. Using the filter: register_block_type_args, and this class is The ezWay to fix that. 

Note: The class's defaults are such that once you set the hook (so to speak), then all blocks will have the HTML Anchor and Additional CSS Class removed. Using the setBlock() method is effectively a whitelist, or at the very least a way to override your defaults on a block by block basis.

That said, the class is flexible. For example, you can make the default to remove only the HTML Anchor control and still allow the Additional CSS Class control to be used, and then whitelist certain blocks to do the opposite.


### The WP Filters

- __register_block_type_args__ - Filters the arguments for registering a block type : https://developer.wordpress.org/reference/hooks/register_block_type_args/


### A Basic Example

```
add_action( 'plugins_loaded', __NAMESPACE__ . '\fn_plugins_loaded' );
function fn_plugins_loaded() {

	// Instantiate the class.
	$new = new BlockAdvanced();

	// Using the *_alt properties (which are both true by default),
	// allow the HTML Anchor and Additional CSS Class controls for this block.
	$new->setBlock( 'core/paragraph' );
	
	// For the image block, allow the Additional CSS Class controls to be used, but not the HTML Anchor control. 'class' is already true via the property bool_custom_class_name_alt.
	$new->setBlock( 'core/image', array( 'anchor' => false ) );

	// Set the hook
	add_filter( 'register_block_type_args', array( $new, 'hookRegisterBlockTypeArgs' ), PHP_INT_MAX, 2 );

}
```

### FAQ

__1) Why?__

The goal of the WPezBlockEditor project is to make it easy to manage and manipulate WordPress blocks (via server-side hooks), mainly to reduce unnecessary UI / UX friction as much as possible. 

In the case of this class, for example, allowing the user to set the HTML Anchor is unnecessary. Anchors for headings? Sure, you bet. Makes sense. But for paragraphs, images and code? Probably not. Along the same lines, if Additional CSS Class controls are not needed why not remove them?

It's important to mention, that this level of control might vary by user role. Authors might get an ultra-minimal UI / UX, while Editors and Admins get a more robust UI / UX. Otherwise, giving Authors access to settings only means they can break things (e.g., add a CSS class where they shouldn't).

__2) OK, what else?__

Overall, the basic philosophy is for these classes to ignore any/all block defaults and start with everything turned off (so to speak). With the initial slate being set to none (again, so to speak), the engineering team can be 100% intentional about the block editor experience they need to craft. Make WP's defaults irrelevant. The only things that matter are what engineering says should matter.

### TODO

- Flesh out the README.md
- More Examples on how to use the different defaults
- For the core/image block, figure out how to remove the Title Attribute control from the Advanced section
- See if other blocks aside from image have one-off-y type controls that might need be removed
- Add a Helpers \ Autoloader


### CHANGE LOG

-v0.0.0 - Tue 3 Oct 2023
  - Finally an initial commit and push.
