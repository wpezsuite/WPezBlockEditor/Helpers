<?php
/**
 * Class BlockAdvance - Used to manipulate (read: remove) the Advanced > Anchor and Advanced > Additional CSS Class controls for WordPress blocks.
 *
 * @package WPezBlockEditor\Helpers\BlockAdvanced
 * @since 0.0.0
 */

namespace WPezBlockEditor\Helpers\BlockAdvanced;

/**
 * Class for manipulating the Advanced settings for blocks.
 */
class ClassBlockAdvanced {

	/**
	 * Bool flag for a block's Advanced > Anchor default setting.
	 *
	 * @var bool
	 */
	protected $bool_anchor_default;

	/**
	 * Bool flag for a block's Advanced > Anchor setting for blocks set individually. Effectively an alternative default.
	 *
	 * @var bool
	 */
	protected $bool_anchor_alt;

	/**
	 * Bool flag for a block's Advanced > customClassName default setting.
	 *
	 * @var bool
	 */
	protected $bool_custom_class_name_default;

	/**
	 * Bool flag for a block's Advanced > customClassName setting for blocks set individually. Effectively an alternative default.
	 *
	 * @var bool
	 */
	protected $bool_custom_class_name_alt;

	/**
	 * Array of block names and their Advanced settings.
	 *
	 * @var array
	 */
	protected $arr_blocks;

	/**
	 * Class constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();
	}

	/**
	 * Set property defaults.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->bool_anchor_default            = false;
		$this->bool_anchor_alt                = true;
		$this->bool_custom_class_name_default = false;
		$this->bool_custom_class_name_alt     = true;
	}

	/**
	 * Set the default value for the class's bool_anchor_default property.
	 *
	 * @param boolean $bool_anchor_default The default value for the class's bool_anchor_default property.
	 *
	 * @return object $this
	 */
	public function setAnchorDefault( bool $bool_anchor_default ): object {

		$this->bool_anchor_default = $bool_anchor_default;
		return $this;
	}

	/**
	 * Get the default value for the class's bool_anchor_default property.
	 *
	 * @return boolean
	 */
	public function getAnchorDefault(): bool {
		return $this->bool_anchor_default;
	}

	/**
	 * Set the default value for the class's bool_anchor_alt property.
	 *
	 * @param boolean $bool_anchor_alt The default value for the class's bool_anchor_alt property.
	 *
	 * @return object $this
	 */
	public function setAnchorAlt( bool $bool_anchor_alt ): object {

		$this->bool_anchor_alt = $bool_anchor_alt;
		return $this;
	}

	/**
	 * Get the default value for the class's bool_anchor_alt property.
	 *
	 * @return boolean
	 */
	public function getAnchorAlt(): bool {
		return $this->bool_anchor_alt;
	}

	/**
	 * Set the default value for the class's bool_custom_class_name_default property.
	 *
	 * @param boolean $bool_custom_class_name_default The default value for the class's bool_custom_class_name_default property.
	 */
	public function setCustomClassNameDefault( bool $bool_custom_class_name_default ): object {

		$this->bool_custom_class_name_default = $bool_custom_class_name_default;
		return $this;
	}

	/**
	 * Get the default value for the class's bool_custom_class_name_default property.
	 *
	 * @return boolean
	 */
	public function getCustomClassNameDefault(): bool {
		return $this->bool_custom_class_name_default;
	}

	/**
	 * Set the default value for the class's bool_custom_class_name_alt property.
	 *
	 * @param boolean $bool_custom_class_name_alt The default value for the class's bool_custom_class_name_alt property.
	 */
	public function setCustomClassNameAlt( bool $bool_custom_class_name_alt ): object {

		$this->bool_custom_class_name_alt = $bool_custom_class_name_alt;
		return $this;
	}

	/**
	 * Get the default value for the class's bool_custom_class_name_alt property.
	 *
	 * @return boolean
	 */
	public function getCustomClassNameAlt(): bool {
		return $this->bool_custom_class_name_alt;
	}

	/**
	 * Get the defaults args for the class's setBlock() method.
	 *
	 * @return array
	 */
	public function getArgsAlts(): array {

		return array(
			'anchor' => $this->bool_anchor_alt,
			'class'  => $this->bool_custom_class_name_alt,
		);
	}



	/**
	 * Set the block and it's arg's to be used by the filter register_block_type_args.
	 *
	 * @param string $str_block The block name.
	 * @param array  $arr_args  The block's args for keys 'anchor' and 'class' (which is ultimately 'customClassName').
	 *
	 * @return object
	 */
	public function setBlock( string $str_block, array $arr_args = array() ): object {

		$arr = array_merge( $this->getArgsAlts(), $arr_args );

		if ( ! is_bool( $arr['anchor'] ) ) {
			$arr['anchor'] = $this->bool_anchor_default;
		}

		if ( ! is_bool( $arr['class'] ) ) {
			$arr['class'] = $this->bool_custom_class_name_default;
		}

		$this->arr_blocks[ trim( $str_block ) ] = $arr;

		return $this;
	}


	/**
	 * Gets a block's args from the property $arr_blocks.
	 *
	 * @param string $str_block The block name.
	 *
	 * @return array
	 */
	public function getBlock( string $str_block ): array {

		if ( isset( $this->arr_blocks[ $str_block ] ) ) {
			return $this->arr_blocks[ $str_block ];
		}

		return array();
	}


	/**
	 * Gets all the blocks from the property $arr_blocks.
	 *
	 * @return array
	 */
	public function getBlocks(): array {
		return $this->arr_blocks;
	}


	/**
	 * Once all your settings are set, call this method using the filter: register_block_type_args.
	 * https:// developer.wordpress.org/reference/hooks/register_block_type_args/
	 *
	 * @param array  $args       An array of args the filter passes in.
	 * @param string $block_name The name of the block the filter passes in.
	 *
	 * @return array
	 */
	public function hookRegisterBlockTypeArgs( array $args = array(), string $block_name = '' ): array {

		// Putting this before the conditional effectively makes this the else part of an else statement.
		// Or see this as the defaults that can be overridden by the conditional.
		$args['supports']['anchor']          = $this->bool_anchor_default;
		$args['supports']['customClassName'] = $this->bool_custom_class_name_default;

		// Now that we've set the else (so to speak), let's check if the block is set individually.
		// The class's defaults effectively make this a whitelist of sorts for the blocks in the $arr_blocks property.
		if ( isset( $this->arr_blocks[ $block_name ] ) ) {

			$args['supports']['anchor']          = $this->arr_blocks[ $block_name ]['anchor'];
			$args['supports']['customClassName'] = $this->arr_blocks[ $block_name ]['class'];
		}

		return $args;
	}
}
