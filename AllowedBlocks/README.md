## Allowed Blocks - TODO

Filter allowed_block_types_all https://developer.wordpress.org/reference/hooks/allowed_block_types_all/

Allows you to specify a list of specific blocks to be allowed in the editor, but it's doesn't allow you to specify a list of blocks to be disallowed. Let's fix that. 