## WPezBlockEditor \ Helpers

__A collection of classes for customizing the WordPress Block Editor UI / UX, and the customizations are not currently supported via the manipulation of the theme.json.__

Please see the README for each class for more details.


### CHANGE LOG

- v0.0.0 - Thur 5 Oct 2023
  - Add the ClassAutoload to autoload the classes. Staying at v0.0.0, for now.

- v0.0.0 - Wed 4 Oct 2023
   - Finally an initial commit and push.
