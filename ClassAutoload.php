<?php

namespace WPezBlockEditor\Helpers;

/**
 * Autoloads classes for WPezBlockEditor\Helpers.
 */
class ClassAutoload {

	/**
	 * Used to store the base directory path.
	 *
	 * @var string
	 */
	private $str_base_dir;

	/**
	 * Prefix to identify the classes we want to autoload.
	 *
	 * @var string
	 */
	private $str_prefix;


	/**
	 * Used to store the paths of the loaded files so we can use require and avoid require_once.
	 *
	 * @var array
	 */
	private static $arr_loaded_paths;

	/**
	 * Class constructor.
	 */
	public function __construct() {

		$this->str_base_dir     = __DIR__;
		$this->str_prefix       = __NAMESPACE__ . '\\';
		self::$arr_loaded_paths = array();

		// Register the autoloader function.
		spl_autoload_register( array( $this, 'autoload' ) );
	}

	/**
	 * The autoloader method.
	 *
	 * @param string $str_class_name The name of the class passed by spl_autoload_register().
	 *
	 * @return void
	 */
	protected function autoload( $str_class_name ) {

		if ( strpos( $str_class_name, $this->str_prefix ) !== 0 ) {
			return;
		}

		// Transform the class name into a file path.
		$new_class_name      = str_replace( $this->str_prefix, '', $str_class_name );
		$str_path_to_require = $this->str_base_dir . DIRECTORY_SEPARATOR . $new_class_name . '.php';
		$str_path_to_require = str_replace( '\\', DIRECTORY_SEPARATOR, $str_path_to_require );

		// Rather than require_once, lets check the "cache" of loaded paths.
		if ( in_array( $str_path_to_require, self::$arr_loaded_paths ) ) {
			return;
		}

		// Check if the file exists.
		if ( file_exists( $str_path_to_require ) ) {
			// Load the class file.
			require $str_path_to_require;
			// "cache" the loaded path.
			self::$arr_loaded_paths[] = $str_path_to_require;
		}
	}
}
